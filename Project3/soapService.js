var SoapMessages= require('./models/SoapMessages');
var AWS = require('aws-sdk')
var ddb = new AWS.DynamoDB();
var uid = require("unique-id-generator");

var soapService = {
  dashboardBackend: {
    dashboardBackendSOAP: {

      clearDB: function(args) {
        var params = {
          TableName: "SoapMessages"
        };
        return {
          name: args.name
        };
      },

      setupSteps: function(args) {
        return {
          name: args.name
        };
      },

      setupComponents: function(args) {
          return {
            name: args.name
          };
        },

        recordEvent: function(args) {
          console.log(args);
          var payload = {
            componentName: args["componentName"],
            versionName: args["versionName"],
            stepName: args["stepName"],
            timeInMS: args["timeInMS"],
            stepResult: args["stepResult"],
            notes: args["notes"]
          }
          saveComponent(payload);
          return {
            name: args.name
          };
        },

        HeadersAwareFunction: function(args, cb, headers) {
          return {
            name: headers.Token
          };
        },
    }
  }
}

  function saveComponent(payload) {
    var sc = new SoftwareComponent({
      _id: uid({prefix:"sc-"}),
      componentName: payload["componentName"],
      versionName: payload["versionName"],
      stepName: payload["stepName"],
      timeInMS: payload["timeInMS"],
      stepResult: payload["stepResult"],
      notes: payload["notes"]
    }); sc.save();
  }

  modul.exports = soapService;
