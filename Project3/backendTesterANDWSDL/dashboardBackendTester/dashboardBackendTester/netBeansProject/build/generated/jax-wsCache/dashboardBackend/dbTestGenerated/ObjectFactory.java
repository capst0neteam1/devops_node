
package dbTestGenerated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the dbTestGenerated package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ReturnValue_QNAME = new QName("http://devops/dashboardBackend/", "returnValue");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: dbTestGenerated
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RecordEvent }
     * 
     */
    public RecordEvent createRecordEvent() {
        return new RecordEvent();
    }

    /**
     * Create an instance of {@link SetupComponents }
     * 
     */
    public SetupComponents createSetupComponents() {
        return new SetupComponents();
    }

    /**
     * Create an instance of {@link ClearDB }
     * 
     */
    public ClearDB createClearDB() {
        return new ClearDB();
    }

    /**
     * Create an instance of {@link RecordEventResponse }
     * 
     */
    public RecordEventResponse createRecordEventResponse() {
        return new RecordEventResponse();
    }

    /**
     * Create an instance of {@link SetupSteps }
     * 
     */
    public SetupSteps createSetupSteps() {
        return new SetupSteps();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://devops/dashboardBackend/", name = "returnValue")
    public JAXBElement<String> createReturnValue(String value) {
        return new JAXBElement<String>(_ReturnValue_QNAME, String.class, null, value);
    }

}
