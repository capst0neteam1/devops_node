/**
 * 
 *         Service: Backend for the DevOps Dashboard Project. 
 *         Describes messages that will be sent to the Dashboard by tools in
 *         a DevOps toolchain.
 *         Version: $Id: dashboardBackend.wsdl 88 2016-09-03 17:41:33Z nwilde $
 *         Author: Norman Wilde
 *     
 * 
 */
@javax.xml.bind.annotation.XmlSchema(namespace = "http://devops/dashboardBackend/")
package dbTestGenerated;
