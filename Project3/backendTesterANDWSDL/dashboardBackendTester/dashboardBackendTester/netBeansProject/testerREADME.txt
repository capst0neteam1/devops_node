$Id: testerREADME.txt 101 2016-09-11 19:21:36Z nwilde $
README file for the Dashboard Backend Tester. 

The tester is run from the command line (eg. a Linux shell window or a
Windows cmd window). You must have Java installed so that you can run
java applications from the command line. To check, give the command:
  java -version

To use the tester:
(1) Copy the jar file (currently named 201609_dashboardBackendTester.jar)
    to a clean directory and cd to that directory.
(2) Get the URL of the runtime wsdl of the service you are testing. This
    will probably be something like:
       http://54.227.38.44:8080/backendStub/dashboardBackend?wsdl
    but the exact name will depend on the names you specified when you
    created your service project.
(3) Give a command similar to (but all on one line):
       java -ea -jar 201609_dashboardBackendTester.jar
         http://54.227.38.44:8080/backendStub/dashboardBackend?wsdl clearDB
(4) To see the usage dump with a list of all tests give the following:
       java -jar 201609_dashboardBackendTester.jar help

Some tests output a dump of all the SOAP messages sent and received in the test
while others output a list of test steps, and save the SOAP messages
to a log file. The test output will tell you which.

 - Norman Wilde, Sept. 2016

 