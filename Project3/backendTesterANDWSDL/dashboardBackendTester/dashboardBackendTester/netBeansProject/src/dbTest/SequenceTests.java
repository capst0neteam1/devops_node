/*
 * For CIS4595C. N. Wilde, September 2016
 * $Id: SequenceTests.java 100 2016-09-11 18:44:44Z nwilde $
 */
package dbTest;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Random;

/**
 * Provides methods to run sequences of tests of the dashboard backend
 *
 * @author nwilde
 */
public class SequenceTests {

    private static final String[] ALL_COMPONENTS = {
        "Connection Manager", "Image Processing", "Customer Management",
        "CC Processing", "DB Utilities MySQL", "DB Utilities Oracle",
        "Authentication", "Data Objects", "Python Interface",
        "Partner Interface", "Messages English", "Messages Spanish",
        "Messages Mandarin", "User Interface", "Admin Interface"
    };
    private static final String[] ALL_VERSIONS = {
        "v.0.2", "v.0.3", "incident response environment", "v.1.0"
    };
    public static final String[] ALL_STEPS = {
        "build", "unit test", "quality assurance scans", "packaging"
    };

    /* an item is a version of a component. We create a list of many items as test data */
    private static ArrayList<TrackedItem> allItems;

    /* create a list of all components combined with all versions */
    static {
        allItems = new ArrayList<TrackedItem>();
        for (int c = 0; c < ALL_COMPONENTS.length; c++) {
            for (int v = 0; v < ALL_VERSIONS.length; v++) {
                TrackedItem ti = new TrackedItem(ALL_COMPONENTS[c], ALL_VERSIONS[v]);
                allItems.add(ti);
            }
        }
    } // end static block

    /**
     * Run a test with one component, one version, 2 events. Prints progress
     * messages showing the steps that have been run
     *
     * @param wsdlURL - WSDL of the service we are testing
     * @param log - saves all SOAP messages to this writer. Caller must open and
     * close this writer.
     * @throws Exception
     */
    public static void testShortSequence(String wsdlURL, PrintWriter log)
            throws Exception {
        /* precondition */
        assert wsdlURL != null;
        assert log != null;
        
        String soapMessages; // soap messages from one operation

        /* first the clearDB operation to start from an empty DB */
        BackendProxy theProxy = new BackendProxy(wsdlURL);
        theProxy.clearDB();
        soapMessages = theProxy.getSOAPMessages();
        log.print(soapMessages);
        log.flush();
        System.out.println("clearDB called");
        /* then setup the list of components */
        theProxy = new BackendProxy(wsdlURL);
        theProxy.setupComponents(ALL_COMPONENTS);
        soapMessages = theProxy.getSOAPMessages();
        log.print(soapMessages);
        log.flush();
        System.out.println("setupComponents called");
        /* then setup the list of steps */
        theProxy = new BackendProxy(wsdlURL);
        theProxy.setupSteps(ALL_STEPS);
        soapMessages = theProxy.getSOAPMessages();
        log.print(soapMessages);
        log.flush();
        System.out.println("setupSteps called");
        /* then two events involving our first TrackedItem */
        TrackedItem ti = allItems.get(0);
        String firstCall = testAnItemOneEvent(ti, wsdlURL, log);
        System.out.println(firstCall);
        String secondCall = testAnItemOneEvent(ti, wsdlURL, log);
        System.out.println(secondCall);
    } // end testShortSequence

    /**
     * Run a test with N random events using different components and versions
     * Prints progress messages showing the steps that have been run
     *
     * @param wsdlURL - WSDL of the service we are testing
     * @param n - number of recordEvent calls in the sequence
     * @param log - saves all SOAP messages to this writer. Caller must open and
     * close this writer.
     * @throws Exception
     */
    public static void testNEventSequence(String wsdlURL, int n, PrintWriter log)
            throws Exception {
        String soapMessages; // soap messages from one operation
        Random rnd = new Random();

        /* first the clearDB operation to start from an empty DB */
        BackendProxy theProxy = new BackendProxy(wsdlURL);
        theProxy.clearDB();
        soapMessages = theProxy.getSOAPMessages();
        log.print(soapMessages);
        System.out.println("clearDB called");
        /* then setup the list of components */
        theProxy = new BackendProxy(wsdlURL);
        theProxy.setupComponents(ALL_COMPONENTS);
        soapMessages = theProxy.getSOAPMessages();
        log.print(soapMessages);
        System.out.println("setupComponents called");
        /* then setup the list of steps */
        theProxy = new BackendProxy(wsdlURL);
        theProxy.setupSteps(ALL_STEPS);
        soapMessages = theProxy.getSOAPMessages();
        log.print(soapMessages);
        System.out.println("setupSteps called");

        /* then N events involving a random TrackedItem */
        for (int count = 0; count < n; count++) {
            TrackedItem ti = allItems.get(rnd.nextInt(allItems.size()));
            String call = testAnItemOneEvent(ti, wsdlURL, log);
            System.out.println(call);
        }

    } // end testNEventSequence

    /**
     * Runs the recordEvent operation for the next step for a given tracked item
     * Does nothing if the tracked item has already gone through all steps of
     * the pipeline
     *
     * @param t - the tracked item that provides the test data
     * @param wsdlURL - WSDL of the dashboard backend we are testing
     * @param aLog - PrintWriter where SOAP messages are logged
     * @return a String with a message saying what was done
     * @throws Exception
     */
    private static String testAnItemOneEvent(TrackedItem t, String wsdlURL, PrintWriter aLog)
            throws Exception {
        final String[] EVENT_RESULTS = {
            "good", "good", "good", "good", "good",
            "doubtful", "doubtful", "doubtful",
            "fail", "fail"
        };
        String componentName = t.getTrackedComponent();
        String versionName = t.getTrackedVersion();
        int step = t.nextStep();
        if (step == -99) {
            return "No more steps for " + t.getItemName();
        }
        String stepName = ALL_STEPS[step];
        long time = System.currentTimeMillis();
        BackendProxy theProxy = new BackendProxy(wsdlURL);
        Random r = new Random();
        String result = EVENT_RESULTS[r.nextInt(EVENT_RESULTS.length)];
        theProxy.recordEvent(componentName, versionName, stepName, time, result, "");
        String soapMessages = theProxy.getSOAPMessages();
        aLog.print(soapMessages);
        aLog.flush();
        return "recordEvent called for " + t.getItemName();
    }

    private static String classTest() {
        String result = "no exceptions thrown";
        // === for the following tests there should be a dashboard backend stub 
        //    service running at the URL given below
        String wsdlURL = "http://localhost:8080/backendStub/dashboardBackend?wsdl";

        // Test T01 shortSequence
        try {
            File f = new File("testLog01.xml");
            PrintWriter p = new PrintWriter(f);
            SequenceTests.testShortSequence(wsdlURL, p);
            p.close();
            System.out.println("T01 - check testLog01.xml to verify SOAP messages");
        } catch (Exception ec) {
            System.out.println("T01 failed");
            result = "fail";
        }
        // Test T02 5 random events
        try {
            File f = new File("testLog02.xml");
            PrintWriter p = new PrintWriter(f);
            SequenceTests.testNEventSequence(wsdlURL, 5, p);
            p.close();
            System.out.println("T02 - check testLog02.xml to verify SOAP messages");
        } catch (Exception ec) {
            System.out.println("T02 failed");
            result = "fail";
        }
        return result;
    } // end ClassTest

    /**
     * Main runs the class test for this class only
     *
     * @param args
     */
    public static void main(String[] args) {
        String testResult = classTest();
        System.out.println("SequenceTests class test: " + testResult);
    }

} // end class SequenceTests
