/*
 * $Id: TrackedItem.java 99 2016-09-10 20:11:38Z nwilde $
 */
package dbTest;

/**
 * Represents a version of a component that is being tracked by the Dashboard.
 * Keeps track of which steps in the pipeline the version has gone through.
 * Helper class for the SequenceTests class
 *
 * @see SequenceTests
 * @author nwilde
 */
public class TrackedItem {

    private String trackedComponent;
    private String trackedVersion;
    private int lastStepNumber;

    public TrackedItem(String compName, String versionName) {
        trackedComponent = compName;
        trackedVersion = versionName;
        lastStepNumber = -1; // initial value so nextStep will start at 0
    } // end constructor

    public String getTrackedComponent() {
        return trackedComponent;
    }

    public String getTrackedVersion() {
        return trackedVersion;
    }

    public String getItemName() {
        return trackedComponent + ":" + trackedVersion;
    }

    /**
     *
     * @return the number of the step for the next event or -99 if this tracked
     * item has already gone through all steps
     */
    public int nextStep() {
        if (lastStepNumber == -99) {
            return -99;
        } else {
            lastStepNumber++;
            if (lastStepNumber < SequenceTests.ALL_STEPS.length) {
                return lastStepNumber;
            } else {
                return -99;
            }
        }
    }
}// end class TrackedItem
