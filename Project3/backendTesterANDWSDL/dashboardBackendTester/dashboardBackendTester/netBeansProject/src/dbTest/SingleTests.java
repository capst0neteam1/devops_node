/*
 * For CIS4595C. N. Wilde, September 2016
 * $Id: SingleTests.java 100 2016-09-11 18:44:44Z nwilde $
 */
package dbTest;

/**
 * Provides methods to run tests of a single operation at a time
 *
 * @author nwilde
 */
public class SingleTests {

    /*
     * Each test returns a String with the messages sent and received
     */
    public static String testSetupComponents(String aWsdl, String[] data)
            throws Exception {
        BackendProxy theProxy = new BackendProxy(aWsdl);
        theProxy.setupComponents(data);
        return theProxy.getSOAPMessages();
    }

    public static String testSetupSteps(String aWsdl, String[] data)
            throws Exception {
        BackendProxy theProxy = new BackendProxy(aWsdl);
        theProxy.setupSteps(data);
        return theProxy.getSOAPMessages();
    }

    public static String testRecordEvent(
            String aWsdl,
            String compName,
            String versName,
            String stepName,
            long timeMS,
            String stepRes,
            String notes)
            throws Exception {
        BackendProxy theProxy = new BackendProxy(aWsdl);
        theProxy.recordEvent(compName, versName, stepName, timeMS, stepRes, notes);
        return theProxy.getSOAPMessages();
    }

    public static String testClearDB(String aWsdl) throws Exception {
        BackendProxy theProxy = new BackendProxy(aWsdl);
        theProxy.clearDB();
        return theProxy.getSOAPMessages();
    }

    /**
     * Simplified test driver. To view the SOAP messages for any particular
     * test, uncomment the print statement for that test.
     * @return 
     */
    private static String classTest() {
        String result = "no exceptions thrown";
        // === for the following tests there should be a dashboard backend stub 
        //    service running at the URL given below
        String wsdlURL = "http://localhost:8080/backendStub/dashboardBackend?wsdl";
        // Test T01 clearDB
        try {
            String msgs01 = SingleTests.testClearDB(wsdlURL);
            //System.out.println(msgs01);
        } catch (Exception ec) {
            System.out.println("T01 failed");
            result = "fail";
        }
        // Test T02 setupComponents
        try {
            String comps[] = {"comp1", "comp2"};
            String msgs02 = SingleTests.testSetupComponents(wsdlURL, comps);
            // System.out.println(msgs02);
        } catch (Exception sc) {
            System.out.println("T02 failed");
            result = "fail";
        }
        // Test T03 setupSteps
        try {
            String steps[] = {"compile", "test", "integrate"};
            String msgs03 = SingleTests.testSetupSteps(wsdlURL, steps);
            //System.out.println(msgs03);
        } catch (Exception ss) {
            System.out.println("T03 failed");
            result = "fail";
        }

        // Test T04 recordEvent
        try {
            String msgs04 = SingleTests.testRecordEvent(wsdlURL, "library2",
                    "v.01", "compile", System.currentTimeMillis(), "doubtful", "one compile error");
            //System.out.println(msgs04);
        } catch (Exception re) {
            System.out.println("T04 failed");
            result = "fail";
        }
        return result;
    } // end ClassTest

    /**
     * Main runs the class test for this class only
     *
     * @param args
     */
    public static void main(String[] args) {
        System.out.println("SingleTest class test starting - checks for exceptions");
        System.out.println(" Uncomment print statements to view detailed results of any test");
        String testResult = classTest();
        System.out.println("SingleTest class test: " + testResult);
    }

} // end class SingleTests
