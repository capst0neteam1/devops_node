/*
 * Message handler resolver class to capture incoming and outgoing SOAP
 * from a service. Works with SaveSoapHandler. N. Wilde, September 2012
 * $Id: SaveSOAPHandlerResolver.java 93 2016-09-03 18:30:37Z nwilde $
 */
package dbTest;


import java.util.ArrayList;
import java.util.List;
import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.HandlerResolver;
import javax.xml.ws.handler.PortInfo;

/**
 * Simplified version of handler resolver from Hewett, p. 276.
 * @author nwilde
 */
public class SaveSOAPHandlerResolver implements HandlerResolver {

    ArrayList<Handler> chain;

    public SaveSOAPHandlerResolver(SaveSoapHandler theHandler){
        chain = new ArrayList<Handler>();
        chain.add(theHandler);
    }

    @Override
    public List<Handler> getHandlerChain(PortInfo portInfo) {
        return chain;
    }

} // end SaveSOAPHandlerResolver class
