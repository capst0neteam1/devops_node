/*
 * For CIS4595C. N. Wilde, September 2016
 * $Id: Main.java 100 2016-09-11 18:44:44Z nwilde $
 */
package dbTest;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import javax.xml.ws.WebServiceException;

/**
 * Main class for the test driver for the dashboard backend service
 * See usage() for how to run it.
 * @author nwilde
 */
public class Main {

    private static final String LOGFILE = "msgLog.xml";
    
    private static final String[] TESTNAMES = {
        "setupComponents", "setupSteps", "recordEvent", "clearDB", 
        "shortSequence", "300Sequence"};
    private static final String[] DESCRIPTIONS = {
        "run only setupComponents",
        "run only setupSteps",
        "run only recordEvent",
        "run only clearDB",
        "test with one component, one version, 2 events",
        "test with many components/versions, 300 events"
    };

    /**
     * Print a usage dump showing how to call this program
     */
    private static void usage() {
        System.out.println("Usage:");
        System.out.println("  java -ea -jar \"201609_dashboardBackendTester.jar\" <url> <testname>");
        System.out.println("where:");
        System.out.println("  <url> is the url of the run-time wsdl of the dashboard backend service");
        System.out.println("  <testname> is one of the following test names:");
        for (int i = 0; i < TESTNAMES.length; i++) {
            System.out.println("    " + TESTNAMES[i] + " - " + DESCRIPTIONS[i]);
        }
    }

    /**
     * Execute the specified test
     * @param url - runtime wsdl url for service being tested
     * @param testNumber identifies which test to run
     */
    private static void runOneTest(String url, int testNumber) {
        String messages = "No messages sent or received";
        try {
            switch (testNumber) {
                /* Cases 0 - 3 are tests of one operation of the service */
                case 0:
                    String[] compNames = {"Component 1", "Component 2", "Component 3"};
                    messages = SingleTests.testSetupComponents(url, compNames);
                    System.out.println("Messages sent and received were");
                    System.out.println(messages);
                    break;
                case 1:
                    String[] stepNames = {"compile", "unit test"};
                    messages = SingleTests.testSetupSteps(url, stepNames);
                    System.out.println("Messages sent and received were");
                    System.out.println(messages);
                    break;
                case 2:
                    String compName = "Component 2";
                    String version = "v0.1.1";
                    String stepName = "unit test";
                    long time = System.currentTimeMillis();
                    String stepResult = "good";
                    String notes = "";
                    messages = SingleTests.testRecordEvent(url, compName, version, stepName, time, stepResult, notes);
                    System.out.println("Messages sent and received were");
                    System.out.println(messages);
                    break;
                case 3:
                    messages = SingleTests.testClearDB(url);
                    System.out.println("Messages sent and received were");
                    System.out.println(messages);
                    break;
                    
                    /* Tests 4 and 5 test sequences of operations */
                case 4:
                    PrintWriter logWriter = Main.openLogFile();
                    SequenceTests.testShortSequence(url, logWriter);
                    logWriter.close();
                    System.out.println("SOAP messages were logged to " + LOGFILE);
                    break;
                case 5:
                    PrintWriter logWriter5 = Main.openLogFile();
                    SequenceTests.testNEventSequence(url, 300, logWriter5);
                    logWriter5.close();
                    System.out.println("SOAP messages were logged to " + LOGFILE);
                    break;
                default:
                    System.out.println("Test not implemented");
                    break;

            } // end switch
        } catch (MalformedURLException ex) {
            System.out.println("Badly formed WSDL URL");
            Main.usage();
            System.exit(1);
        } catch (WebServiceException ex) {
            System.out.println("Could not get a dashboard backend WSDL at that URL");
            Main.usage();
            System.exit(1);
        } catch (Exception e) {
            System.out.println("Unexpected exception");
            e.printStackTrace();
            System.exit(1);
        }

        return;
    } // end runOneTest

    private static PrintWriter openLogFile() throws FileNotFoundException{
        File f = new File (LOGFILE);
        PrintWriter p = new PrintWriter(f);
        return p;
    } // openLogFile
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        if (2 != args.length) {
            Main.usage();
            System.exit(1);
        } // end if
        String wsdlURL = args[0];
        String testName = args[1];
        for (int i = 0; i < TESTNAMES.length; i++) {
            if (testName.equalsIgnoreCase(TESTNAMES[i])) {
                Main.runOneTest(wsdlURL, i);
                return;
            }
        }// end for
        System.out.println("No such test - " + testName);
        Main.usage();
        return;
    }

} // end class Main
