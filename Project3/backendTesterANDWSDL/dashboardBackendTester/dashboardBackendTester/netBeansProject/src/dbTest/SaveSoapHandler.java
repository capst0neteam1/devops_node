/*
 * Message handler class to capture incoming and outgoing SOAP
 * from a service. N. Wilde, September 2012
 * $Id: SaveSoapHandler.java 93 2016-09-03 18:30:37Z nwilde $
 */
package dbTest;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Set;
import javax.xml.namespace.QName;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

/**
 * A message handler for JAX-WS that captures the contents of SOAP messages.
 * To use it, construct a SaveSOAPHandler, use a SaveSOAPHandlerResolver
 * to attach it to the service, and then run the service. When done,
 * call getAccumulatedSOAPMessages() to get a string with the messages.
 * <p>Note: All accumulated messages are stored in memory, so this
 * class should be used during debugging runs, not for a production
 * service</p>
 * <p>See <i>Java SOA Cookbook</i> by Eben Hewett, section 6.11 for
 * explanations of the techniques used in this class.</p>
 * @author nwilde
 */
public class SaveSoapHandler implements SOAPHandler<SOAPMessageContext> {

    /** used to store the SOAP messages */
    private ByteArrayOutputStream mySoapStream;

    /**
     * Constructor - creates the ByteArray that will hold messages
     */
    public SaveSoapHandler() {
        mySoapStream = new ByteArrayOutputStream();
    } // end constructor

    @Override
    public Set<QName> getHeaders() {
        return null;
    }

    @Override
    public boolean handleMessage(SOAPMessageContext ctx) {
        try {
            /* apparently the only way to capture a message is to
             * tell it to write itself to an output stream as follows.
             */
            ctx.getMessage().writeTo(mySoapStream);
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return true;
    } // end handleMessage

    @Override
    public boolean handleFault(SOAPMessageContext ctx) {
        return false;
    }

    @Override
    public void close(MessageContext context) {
    }

    /**
     * Get the SOAP messages that have passed through this handler
     * @return a string with the messages in raw XML format
     */
    public String getAccumulatedSOAPMessages() {
        try {
            mySoapStream.flush(); // probably unneeded, but just in case ...
        } catch (IOException e) {
            System.out.println(e.toString());
        }
        String rawSOAP = mySoapStream.toString();
        return rawSOAP;
    }

} // end class SaveSOAPHandler
