var temp;

//object template, to make things easier :)
exports.item = function (id, title, desc, cost, commish, image) {
    
    temp = {
        id: id,
        title: title,
        desc: desc,
        cost: cost,
        commish: 0.5,
        image: image
    };
    
    return temp;
    
};